## General Intro

> The single most important skill for a computer scientist is **problem solving**. Problem solving means the ability to formulate problems, think creatively about solutions, and express a solution clearly and accurately

An **algorithm **is a step by step list of instructions that if followed exactly will solve the problem under consideration.

### Programming languages

#### Low-level languages \(aka. machine languages\)

Machine language is the encoding of instructions in binary so that they can be directly executed by the computer. Assembly language uses a slightly easier format to refer to the low level instructions. Loosely speaking, computers can only execute programs written in low-level languages. To be exact, computers can actually only execute programs written in machine language.

#### High-level languages

Thus, programs written in a high-level language \(and even those in assembly language\) have to be processed before they can run. This extra processing takes some time, which is a small disadvantage of high-level languages.

* It is much easier to program in high-level languages
* igh-level languages are portable, meaning that they can run on different kinds of computers with few or no modifications

#### Translating high- to low-level

* interpreters - line by line
* compilers - turns high-level source code into low-level object code AKA. executable.

_Many modern languages use both processes_. They are first **compiled** into a lower level language, called byte code, and **then interpreted** by a program called a virtual machine. Python uses both processes, but because of the way programmers interact with it, it is usually considered an interpreted language.

### Comments

```python
# Usual comments in Python

#---------------------------------------------------
# This is often at the beginning of programs
# It usually says what the progr is about
# and who made it, etc
#---------------------------------------------------
'''
docstring comments and multi-line comments
'''
```



